package com.delta.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController2 {

    @GetMapping("/2") // path we want to use
    @ResponseBody // Displays what we return in our homepage
    public String home2() {
        return "Welcome to my 2nd Landing Page!";
    }
}
