package com.delta.springnotes.controllers;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ThymeleafHome2 {
   @GetMapping("/thymeleaf-home2")
    public String welcomehome() {
       return "thymeleaf-view2"; //return the name of our view "thymeleaf-view2"
   }
}
