package com.delta.springnotes.controllers;

import com.delta.springnotes.models.Book;
import com.delta.springnotes.repos.BookRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class BookController {

    // dependency injection
    private final BookRepository bookRepository;

    public BookController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    // METHODS
    // get all books and display them on books/index view
    @GetMapping("/books")
    public String showBooks(Model model) {
        List<Book> bookList = bookRepository.findAll();

        model.addAttribute("noBooksFound", bookList.size() == 0);
        model.addAttribute("books", bookList);

        return "books/index"; // name of the view books/index
    }

    // add a book form
    @GetMapping("/books/create")
    public String showForm(Model model) {
        model.addAttribute("newBook", new Book());

        return "books/create";
    }

    @PostMapping("/books/create")
    public String createBook(@ModelAttribute Book bookToCreate) {
        // save the bookToCreate parameter
        bookRepository.save(bookToCreate);

        // redirect user to the list of books
        return "redirect:/books/";
    }

    // method to display individual book
    @GetMapping("/books/{id}")
    public String showBook(@PathVariable long id, Model model) {
        Book book = bookRepository.getOne(id); // getting book based on id

        model.addAttribute("showBook", book);

        return "books/show";
    }

    // method to allow user to edit book info
    @GetMapping("/books/{id}/edit")
    public String showEdit(@PathVariable long id, Model model) {
        // find a book with the passed in id
        Book bookToEdit = bookRepository.getOne(id);
        model.addAttribute("editBook", bookToEdit);

        return "books/edit";
    }

    // method to process the edited info
    @PostMapping("/books/{id}/edit")
    public String updateBook(@PathVariable long id,
                             @RequestParam(name = "title")String title,
                             @RequestParam(name = "author")String author,
                             @RequestParam(name = "genre")String genre,
                             @RequestParam(name = "releaseDate")String releaseDate)
    {
        // find the book with the passed in id
        Book foundBook = bookRepository.getBookById(id);

        // update the book's info
        foundBook.setTitle(title);
        foundBook.setAuthor(author);
        foundBook.setGenre(genre);
        foundBook.setReleaseDate(releaseDate);

        // save the new book's data changes
        bookRepository.save(foundBook);

        // redirect the user to the url that contains the list of books
        return "redirect:/books/";
    }

    // method to allow user to delete books
    @PostMapping("/books/{id}/delete")
    public String delete(@PathVariable long id) {
        // access the deleteById() from the repository
        bookRepository.deleteById(id);

        // redirect to url with list of books
        return "redirect:/books/";
    }
}
