package com.delta.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class HelloController {

    @GetMapping("/hello")
    @ResponseBody
    public String hello() {
        return "Hello!";
    }

    // example of path variables: variables that are part of our URL
    @GetMapping("/hello/{name}") //{name} is our path variable with curly braces
    @ResponseBody
    public String sayHello(@PathVariable String name) {
        return "Hello there, " + name + "!";
    }

    // path variable that is not a string
    @RequestMapping(path = "/increment/{number}", method = RequestMethod.GET)  // longer variation of getmapping - how you assign a URL: path = then specified URL and what kind of method
    @ResponseBody
    public String addingOne(@PathVariable int number){
        return number + " plus one is " + (number + 1) + "!";
    }

} // end of class
