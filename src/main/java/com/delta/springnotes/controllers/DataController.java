package com.delta.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class DataController {
    // PASSING DATA TO OUR VIEWS

    // create a method that will accept a model
    // model - represents our data that will be displayed in our view
    @GetMapping("/data/{name}")
    public String helloName(@PathVariable String name, Model model) {
        model.addAttribute("displayName", name);
        return "data-view"; // data-view is going to be the name of our view
    }
}
