package com.delta.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class JoinController {

    // assign a specific URL
    @GetMapping("/join")
    public String showForm() {
        return "join-view"; // "join-view" = name of our view
    }

    // second method: set up a POST method for our view's data (form)
    @PostMapping("/join")
    public String displayData(@RequestParam(name = "classroom")String classroom, Model model ) {
      model.addAttribute("classroom", "Welcome to " + classroom);
      return "join-view";
    }

}
