package com.delta.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class JoinController2 {
    // assign a specific url
    @GetMapping("/join2")
    public String showMyForm(){return "join-view2";} // "join-view2" = name of our view

    // second method: set up a POST method for our view's data (form)
    @PostMapping("/join2")
    public String displayMyData(@RequestParam(name = "practice")String practice, Model model) {
        model.addAttribute("practice", "Welcome to my practice " + practice + " ! ");
        return "join-view2";
    }
}
