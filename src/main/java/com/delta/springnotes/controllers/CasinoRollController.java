package com.delta.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Random;

@Controller
public class CasinoRollController {



//    @GetMapping("/casino-roll")
//    public String showCasinoRoll() {
//        return "guess-roll";
//    }

    // create a method to tell us if we guessed correct or not
//    @GetMapping("/casino-roll/{num}")
//    public String userGuessedNumber(@PathVariable int num, Model model) {
//
//        int correctNumber = (int) (Math.random()*(5) + 1);
//
//        String message;
//
//        if (correctNumber == num) {
//            message = "YAY!! You guessed correctly!";
//        }else {
//            message = " Sorry! You guessed wrong!";
//        }
//
//        model.addAttribute("showcaseMessage", message); // shows message variable on our view
//        return "guess-roll";
//    }
// ========================== SECOND SOLUTION =======================================================
//
    @GetMapping("/casino-roll") // url
    public String showCasinoRoll(){
        return "guess-roll";
    }

    @PostMapping("/casino-roll")
    public String userGuessedNumber(@RequestParam String userGuess, Model model) {
        Random random = new Random(); // initializing random class

        //variable for random number
        int newRandom = random.nextInt(6)+1; // outputting numbers 1-6

        // add attributes for the view
        model.addAttribute("thisIsTheUserGuess", userGuess);
        model.addAttribute("random", newRandom);

        //return to a view
        return "guess-roll";
    }


}
