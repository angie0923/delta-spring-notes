package com.delta.springnotes.controllers;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ThymeleafHome {
    @GetMapping("/thymeleaf-home")
    public String welcome() {
        return "thymeleaf-view"; // return the name of our view "thymeleaf-view"
    }
}
