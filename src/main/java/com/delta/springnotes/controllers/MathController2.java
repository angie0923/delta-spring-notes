package com.delta.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MathController2 {
    @GetMapping("/add2/{num1}/and/{num2}") // url we want to use
    @ResponseBody //displays our data
    public String addNum(@PathVariable int num1, @PathVariable int num2) {
        return num1 + "plus" + num2 + "is" + (num1 + num2);
    }

    @GetMapping("/sub/{num1}/{num2}")
    @ResponseBody
    public String sub(@PathVariable int num1, @PathVariable int num2) {
        return num2 + " minus " +num1+ " is " + (num2 - num1);
    }

    @GetMapping("/multiply/{num1}/{num2}")
    @ResponseBody
    public int multiply(@PathVariable int num1, @PathVariable int num2) {
        return num1 * num2;
    }

    @GetMapping("/divide/{num1}/{num2}")
    @ResponseBody
    public int divide(@PathVariable int num1, @PathVariable int num2) {
        return num1 / num2;
    }

}
