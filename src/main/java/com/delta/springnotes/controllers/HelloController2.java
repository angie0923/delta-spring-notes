package com.delta.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController2 {

    @GetMapping("/hello2") // path we want to use
    @ResponseBody // Displays our page
    public String hello() {
        return "Hello again!!";
    }
}
