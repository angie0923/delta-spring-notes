package com.delta.springnotes.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

// @Controller
@Controller
public class HomeController {

    @GetMapping("/") // path we want to use
    @ResponseBody
    // Displays what we return in our homepage
    public String home(){
        return "Welcome to my Landing Page!";
    }


}
