package com.delta.springnotes.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class MathController {
    // add method

    @GetMapping("/add/{num1}/and/{num2}")
    @ResponseBody
    public String add(@PathVariable int num1, @PathVariable int num2 ){
        return num1 + "plus" +num2 + "is" + (num1 + num2);}


    @GetMapping ("/subtract/{num1}/from/{num2}")
    @ResponseBody
    public String subtract(@PathVariable int num2, @PathVariable int num1) {
        return num2 + " minus " + num1 + " is " + (num2 - num1);
    }

    @GetMapping("sub/{num2}/{num1}")
    @ResponseBody
    public int sub(@PathVariable int num1, @PathVariable int num2) {
        return num2 - num1;
    }

    @GetMapping("/multiply/{number1}/{number2}")
    @ResponseBody
    public int multiply(@PathVariable int number1, @PathVariable int number2) {
        return number1 * number2;
    }

    @GetMapping("/divide/{number1}/{number2}")
    @ResponseBody
    public int divide(@PathVariable int number1, @PathVariable int number2) {
        return number1 / number2;
    }



}


