package com.delta.springnotes.controllers;

import com.delta.springnotes.models.Employee;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class EmployeeController {

    @GetMapping("/employees")
    // this model allows us to use the model framework
    public String getEmployees(Model model) {
        List<Employee> employeeList = new ArrayList<>();

        employeeList.add(new Employee("Abby", "abby@email.com", "ADMIN"));
        employeeList.add(new Employee("Bryant", "bryant@email.com", "ADMIN"));
        employeeList.add(new Employee("Crystal", "crystal@email.com", "USER"));
        employeeList.add(new Employee("David", "david@email.com", "USER"));

        model.addAttribute("employees", employeeList);

        return "list-of-employees-view";

    }
}
