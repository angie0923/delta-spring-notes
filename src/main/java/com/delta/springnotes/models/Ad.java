package com.delta.springnotes.models;

import javax.persistence.*;

@Entity
@Table(name = "ads")
public class Ad {


    // properties/fields
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false, length = 100)
    private String title;

    @Column(nullable = false) //nullable = false means the column can't be left blank
    private String description;


    // CONSTRUCTOR
    public Ad(long id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    public Ad() { // spring needs an empty constructor

    }


    // GETTERS / SETTERS
    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
