package com.delta.springnotes.models;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {

    // variables
    @Id
    @GeneratedValue
    private long id;
    @Column(nullable = false, length = 300, unique = true)
    private String username;
    @Column(nullable = false, length = 300, unique = true)
    private String email;
    @Column(nullable = false, length = 300)
    private String password;

    public User() {

    }

    // copy constructor
    public User(User copy) {
        id = copy.id; // this line is super important!!! many things won't work if absent
        email = copy.email;
        username = copy.username;
        password = copy.password;
    }

}
