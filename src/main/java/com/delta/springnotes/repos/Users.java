package com.delta.springnotes.repos;

import com.delta.springnotes.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Users extends JpaRepository<User, Long> {

    User findByUsername(String username);

}
